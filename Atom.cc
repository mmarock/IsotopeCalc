#include "Atom.hh"

#include "Logger.hh"

namespace qm
{

Isotope::Isotope(uint A, double re_atmass, double isotopic_comp,
                 double stdatomic_weight, uint8_t n)
    : A(A), rel_atomic_mass(re_atmass), isotopic_comp(isotopic_comp),
      std_atomic_weight(stdatomic_weight), notes(n)
{
    logger::log(logger::LVL_DEBUG,
                "\tIsotope: added %u,%lf,%lf,%lf,%u", A, re_atmass,
                isotopic_comp, stdatomic_weight, n);
}

Atom::Atom(uint Z, std::string symbol) : Z_(Z), symbol_(symbol)
{
    logger::log(logger::LVL_DEBUG, "Atom: added %u,%s", Z_,
                symbol_.c_str());
}

Atom::uint Atom::Z() const noexcept { return Z_; }

const std::string &Atom::symbol() const noexcept { return symbol_; }

void Atom::addIsotope(uint A, double rel_atomic_mass,
                      double isotopic_comp, double std_atomic_weight,
                      uint8_t notes)
{
    isotopes_.emplace_back(A, rel_atomic_mass, isotopic_comp,
                           std_atomic_weight, notes);
}

const std::vector<Isotope> &Atom::isotopes() const noexcept
{
    return isotopes_;
}

} // namespace qm
