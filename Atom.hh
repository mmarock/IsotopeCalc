#ifndef ATOM_HH
#define ATOM_HH

#include <cstdint>

#include <string>
#include <vector>

namespace qm
{

enum NOTES : uint8_t
{
    N_m = 1 << 0,
    N_g = 1 << 1,
    N_r = 1 << 2
};

struct Isotope
{
    using uint = uint16_t;

    uint A;
    double rel_atomic_mass;
    double isotopic_comp;
    double std_atomic_weight;

    uint8_t notes;

    Isotope(uint A, double re_atmass, double isotop_comp,
            double stdatomic_weight, uint8_t notes);
};

class Atom
{
public:
    using uint = uint16_t;

private:
    uint Z_;
    std::string symbol_;

    std::vector<Isotope> isotopes_;

public:
    Atom(uint Z, std::string symbol);

    uint Z() const noexcept;
    const std::string &symbol() const noexcept;

    void addIsotope(uint A, double rel_atomic_mass,
                    double isotopic_comp, double std_atomic_weight,
                    uint8_t notes);

    const std::vector<Isotope> &isotopes() const noexcept;
};

} // namespace qm

#endif // ATOM_HH
