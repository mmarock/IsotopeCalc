#include "AtomCalculator.hh"

#include <algorithm>

#include <cassert>
#include <unistd.h>

#include "Logger.hh"

namespace qm
{

AtomCalculator::AtomCalculator(const Atom &ref,
                               const Isotope &isotope, int charge,
                               double energy_in_mev,
                               double injection_voltage,
                               double magnet_radius,
                               double mass_error)
    : energy_in_mev_(energy_in_mev),
      injection_voltage_(injection_voltage),
      magnet_radius_(magnet_radius),
      terminal_voltage_(calc_terminal_voltage(
          energy_in_mev, injection_voltage, charge)),
      magnetic_field_(calc_magnetic_field(
          ref, isotope, charge, magnet_radius, energy_in_mev)),
      mass_error_(mass_error)
{
    using namespace logger;
    log(LVL_INFO,"magnetic-field:%.02lfGauss, terminal-voltage:%.02lfMV",
        magnetic_field_ * 10000, terminal_voltage_);
}

AtomCalculator::~AtomCalculator() {}

const double &AtomCalculator::energyInMeV() const noexcept
{
    return energy_in_mev_;
}

const double &AtomCalculator::injectionVoltage() const noexcept
{
    return injection_voltage_;
}

const double &AtomCalculator::terminalVoltage() const noexcept
{
    return terminal_voltage_;
}

const double &AtomCalculator::magneticField() const noexcept
{
    return magnetic_field_;
}

std::vector<Calculator::Result>
AtomCalculator::query(const Atom &a) const
{
    using namespace logger;

    std::vector<Calculator::Result> r;

    for (const auto &iso : a.isotopes())
    {
        for (int charge = 1; charge <= a.Z(); ++charge)
        {
            auto mass =
                kg_to_amu(std::pow(charge * magnetic_field_ * magnet_radius_,
                         2.0) * electron_charge /
                2.0 /
                (terminal_voltage_ * (charge + 1) +
                 injection_voltage_) / 1E6) + mev_to_amu(electron_mass_in_mev(charge));

            log(LVL_DEBUG, "Z:%d, A:%d, mass:%lf+-%lf charge:%d", a.Z(),
                iso.A, iso.rel_atomic_mass, mass_error_, charge);

            // down limit
            if (iso.rel_atomic_mass < mass - mass_error_)
            {
                continue;
            }
            // upper limit
            if (iso.rel_atomic_mass > mass + mass_error_)
            {
                continue;
            }
            r.emplace_back(
                a.Z(), iso, charge,
                (charge + 1) * terminal_voltage_ + injection_voltage_,
                std::fabs(iso.rel_atomic_mass - mass) / iso.rel_atomic_mass);
        }
    }

    return r;
}

double AtomCalculator::calc_terminal_voltage(double energy_in_mev,
                                             double injection_voltage,
                                             int charge)
{
    return (energy_in_mev - injection_voltage) / (charge + 1);
}

double AtomCalculator::calc_magnetic_field(const Atom &ref,
                                           const Isotope &isotope,
                                           int charge,
                                           double magnet_radius,
                                           double energy_in_mev)
{
    constexpr double speed_of_light = 299792458;

    assert(charge <= ref.Z() && "charge bigger than Z");

    using namespace logger;
    //const auto mass_in_mev = amu_to_mev(isotope.rel_atomic_mass) -
      //                       electron_mass_in_mev(charge);
    const auto mass_in_mev = isotope.rel_atomic_mass * 931.5 - charge * 0.511;

    return std::sqrt(2 * mass_in_mev * energy_in_mev) /
           (charge * magnet_radius * speed_of_light) * 1E6;

    /*
    return std::sqrt(std::pow(energy_in_mev + mass_in_mev, 2.0) -
                     std::pow(mass_in_mev, 2.0)) /
           (charge * magnet_radius * speed_of_light) * 1E6;
           */
}

double AtomCalculator::calc_comp_mass(const Isotope &i, int charge)
{
    return i.rel_atomic_mass +
           mev_to_amu(electron_mass_in_mev(charge));
}

constexpr double AtomCalculator::electron_mass_in_mev(int units)
{
    return units * 0.5109989461;
}

constexpr double AtomCalculator::mev_to_amu(double mev)
{
    return mev / 931.49;
}

constexpr double AtomCalculator::amu_to_mev(double amu)
{
    return amu / mev_to_amu(1);
}

constexpr double AtomCalculator::kg_to_amu(double kg)
{
    return kg / 1.660540E-27;
}

} // namespace qm
