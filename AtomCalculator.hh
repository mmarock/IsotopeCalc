#ifndef ATOMCALCULATOR_HH
#define ATOMCALCULATOR_HH

#include "Calculator.hh"

#include <cmath>

namespace qm
{

class AtomCalculator : public Calculator
{
    double energy_in_mev_;
    double injection_voltage_;
    double magnet_radius_;

    double terminal_voltage_;
    double magnetic_field_;

    double mass_error_;

public:
    AtomCalculator(const Atom &ref, const Isotope &isotope, int charge,
                   double energy_in_mev, double injection_voltage,
                   double magnet_radius, double mass_error);

    ~AtomCalculator();

    const double &energyInMeV() const noexcept;
    const double &injectionVoltage() const noexcept;
    const double &terminalVoltage() const noexcept;
    const double &magneticField() const noexcept;

    virtual std::vector<Result> query(const Atom &a) const override;

private:
    static double calc_terminal_voltage(double energy_in_mev,
                                        double injection_voltage,
                                        int charge);
    static double calc_magnetic_field(const Atom &ref,
                                      const Isotope &isotope,
                                      int charge,
                                      double magnet_radius, double energy_in_mev);
    static double calc_comp_mass(const Isotope &i, int charge);

    static constexpr double electron_charge = 1.60217662E-19;
    static constexpr double electron_mass_in_mev(int units);
    static constexpr double mev_to_amu(double mev);
    static constexpr double amu_to_mev(double amu);
    static constexpr double kg_to_amu(double kg);
};

} // namespace qm

#endif // ATOMCALCULATOR_HH
