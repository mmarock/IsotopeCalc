#include "AtomImporter.hh"

#include <cassert>

#include "Atom.hh"

namespace qm
{

AtomImporter::AtomImporter() {}
AtomImporter::~AtomImporter() {}

std::vector<Atom> AtomImporter::read() const
{
    assert(0 && "should never get here");
    return std::vector<Atom>();
}

} // namespace qm
