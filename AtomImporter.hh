#ifndef ATOMIMPORTER_HH
#define ATOMIMPORTER_HH

#include <vector>

namespace qm
{

class Atom;

class AtomImporter
{
public:
    AtomImporter();
    virtual ~AtomImporter() = 0;

    virtual std::vector<Atom> read() const = 0;
};

} // namespace qm

#endif // ATOMIMPORTER_HH
