#include "Calculator.hh"

#include <cassert>

namespace qm
{

Calculator::Result::Result(Atom::uint Z, const Isotope &i, int charge,
                           double en, double mass_dev)
    : Z(Z), isotope(i), charge(charge), energy(en),
      mass_deviation(mass_dev)
{
}

Calculator::Result::~Result() {}

Calculator::Calculator() {}

Calculator::~Calculator() {}

std::vector<Calculator::Result> Calculator::query(const Atom &a) const
{
    (void)a;
    assert(0 && "must never get here");
    return std::vector<Calculator::Result>();
}

} // namespace qm
