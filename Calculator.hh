#ifndef CALCULATOR_HH
#define CALCULATOR_HH

#include "Atom.hh"

namespace qm
{

class Calculator
{
public:
    struct Result
    {
        Atom::uint Z;
        Isotope isotope;
        int charge;
        double energy;
        double mass_deviation;

        Result(Atom::uint Z, const Isotope &i, int charge,
               double energy, double mass_dev);
        ~Result();
    };

public:
    Calculator();
    virtual ~Calculator() = 0;

    virtual std::vector<Result> query(const Atom &a) const = 0;

}; // class Calculator

} // namespace qm

#endif // CALCULATOR_HH
