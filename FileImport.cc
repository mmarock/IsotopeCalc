#include "FileImport.hh"

#include "Atom.hh"
#include "Logger.hh"

#include <cassert>
#include <cstdio>
#include <cstring>

#include <unistd.h>

#include <iostream>
#include <regex>
#include <stdexcept>

namespace qm
{

////////////////////////////////////////////////////////////
//      Exceptions
////////////////////////////////////////////////////////////

class FileNotFound : public std::runtime_error
{
public:
    FileNotFound(const std::string &file)
        : std::runtime_error("FileNotFound: " + file)
    {
    }
};

enum class TOKEN : uint8_t
{
    AtomicNumber,
    AtomicSymbol,
    MassNumber,
    RelativeAtomicMass,
    IsotopicComposition,
    StdAtomicWeight,
    Notes,
    Last
};

static const char *get_str(TOKEN t)
{
    switch (t)
    {
    case TOKEN::AtomicNumber:
        return "Atomic Number";
    case TOKEN::AtomicSymbol:
        return "Atomic Symbol";
    case TOKEN::MassNumber:
        return "Mass Number";
    case TOKEN::RelativeAtomicMass:
        return "Relative Atomic Mass";
    case TOKEN::IsotopicComposition:
        return "Isotopic Composition";
    case TOKEN::StdAtomicWeight:
        return "Standard Atomic Weight";
    case TOKEN::Notes:
        return "Notes";
    case TOKEN::Last:
        assert(0 && "should never get here");
    };
    return "invalid value";
}

/*
static bool strequal(TOKEN t, const char *buf, std::size_t buf_n)
{
    return !strncmp(get_str(t), buf, buf_n);
}
*/

static const std::string &get_regex()
{
    static std::string reg = []() -> std::string {
        std::string init;
        for (uint8_t i = static_cast<uint8_t>(TOKEN::AtomicNumber);
             i < static_cast<uint8_t>(TOKEN::Last); ++i)
        {
            init += get_str(static_cast<TOKEN>(i));
            init += " = *\\[?([a-zA-Z0-9\\.]*).*\n?";
        }
        using namespace logger;
        log(LVL_DEBUG, "use regex: '%s'", init.c_str());
        return init;
    }();
    return reg;
}

////////////////////////////////////////////////////////////
//      FileImport definition
////////////////////////////////////////////////////////////
FileImport::FileImport(const std::string &path_to_file)
    : path_to_file_(path_to_file)
{
}

FileImport::~FileImport() {}

std::vector<Atom> FileImport::read() const
{
    using namespace logger;
    std::vector<Atom> r;

    FILE *fp = fopen(path_to_file_.c_str(), "r");
    if (!fp)
    {
        throw FileNotFound(path_to_file_);
    }
    log(LVL_DEBUG, "%s opened", path_to_file_.c_str());

    char *buf(nullptr);
    size_t buf_n(0);

    std::string atom_buffer;
    while (getline(&buf, &buf_n, fp) > 0)
    {
        if (strlen(buf) > 1)
        {
            atom_buffer += std::string(buf);
            continue;
        }

        // next newline, now regex the data
        std::regex re(get_regex());
        std::smatch match;

        std::regex_search(atom_buffer, match, re);

        Atom::uint Z = std::stoul(match[1].str());

        auto it =
            std::find_if(r.begin(), r.end(),
                         [&Z](const Atom &a) { return a.Z() == Z; });
        if (it == r.end())
        {
            r.emplace_back(Z, match[2].str());
            it = r.end() - 1;
        }

        // now add the isotope
        Atom::uint A = std::stoul(match[3].str());
        double rel_atomic_mass = std::stod(match[4].str());
        // double rel_atomic_mass = 0;
        double isotopic_comp = (match[5].str().size() > 0)
                                   ? std::stod(match[5].str())
                                   : 0;
        // double isotopic_comp = 0;
        double std_atomic_weight = (match[6].str().size() > 0)
                                       ? std::stod(match[6].str())
                                       : 0;
        // TODO compile Notes section correct
        it->addIsotope(A, rel_atomic_mass, isotopic_comp,
                       std_atomic_weight, 0);

        atom_buffer.clear();

    }

    fclose(fp);

    if (buf)
    {
        free(buf);
    }
    return r;
}

} // namespace qm
