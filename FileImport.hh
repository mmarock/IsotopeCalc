#ifndef FILEIMPORT_HH
#define FILEIMPORT_HH

#include <string>
#include <vector>

#include "AtomImporter.hh"

namespace qm
{

class FileImport : public AtomImporter
{
    std::string path_to_file_;

public:
    FileImport(const std::string &path_to_file);
    ~FileImport();

    virtual std::vector<Atom> read() const override;
};

} // namespace qm

#endif // FILEIMPORT_HH
