#include "Logger.hh"

#include <cstdarg>
#include <cstdio>

namespace logger
{

static LEVEL lvl_ = LEVEL::LVL_INFO;

static const char *get_prio(LEVEL lvl)
{
    switch (lvl)
    {
    case LEVEL::LVL_EMERG:
        return "EMERG";
    case LEVEL::LVL_ALERT:
        return "ALERT";
    case LEVEL::LVL_CRIT:
        return "CRIT";
    case LEVEL::LVL_ERR:
        return "ERR";
    case LEVEL::LVL_WARNING:
        return "WARN";
    case LEVEL::LVL_NOTICE:
        return "NOTICE";
    case LEVEL::LVL_INFO:
        return "INFO";
    case LEVEL::LVL_DEBUG:
        return "DEBUG";
    };
}

void set_level(LEVEL lvl) noexcept { lvl_ = lvl; }

LEVEL get_level() noexcept { return lvl_; }

void log(LEVEL lvl, const char *format, ...)
{
    if (lvl > lvl_)
    {
        return;
    }

    char fmt_buf[1024];
    snprintf(fmt_buf, sizeof(fmt_buf), "[ %s ]\t%s\n", get_prio(lvl), format);

    va_list ar;
    va_start(ar, format);
    vprintf(fmt_buf, ar);
    va_end(ar);
}

} // namespace logger
