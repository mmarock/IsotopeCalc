#ifndef LOGGER_HH
#define LOGGER_HH

#include <cstdint>

namespace logger
{

enum LEVEL : uint8_t
{
    LVL_EMERG = 0,
    LVL_ALERT,
    LVL_CRIT,
    LVL_ERR,
    LVL_WARNING,
    LVL_NOTICE,
    LVL_INFO,
    LVL_DEBUG
};

void set_level(LEVEL lvl) noexcept;
LEVEL get_level() noexcept;

void log(LEVEL LVL, const char *format, ...);

} // namespace logger

#endif // LOGGER_HH
