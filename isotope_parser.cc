#include "isotope_parser.hh"

#include <regex>

#include "Logger.hh"

namespace isoparser
{

int parse(const std::string &str,
          const std::vector<qm::Atom> &atom_db, Result &r)
{

    using namespace logger;
    std::regex rg("^(\\d+)([a-zA-Z]+)(\\d+)\\+\\((.+)MeV\\)$");

    std::smatch match;

    if (!std::regex_match(str, match, rg) || match.size() != 5)
    {
        log(LVL_ERR, "wrong format, example: 4He2+(1MeV)");
        return -1;
    }

    auto atom = std::find_if(
        atom_db.begin(), atom_db.end(), [&](const qm::Atom &a) {
            auto lhs = a.symbol().c_str();
            auto rhs = match[2].str().c_str();
            return !strncmp(lhs, rhs, std::strlen(rhs));
        });
    if (atom == atom_db.end())
    {
        log(LVL_ERR, "ion %s not found in db",
            match[2].str().c_str());
        return -1;
    }
    r.Z = atom->Z();
    auto A = std::stoul(match[1]);
    auto iso =
        std::find_if(atom->isotopes().begin(), atom->isotopes().end(),
                     [&](const qm::Isotope &i) { return i.A == A; });
    if (iso == atom->isotopes().end())
    {
        log(LVL_ERR, "ion %s: isotope A=%d not found",
            match[2].str().c_str(), A);
    }
    r.A = A;

    r.charge = std::stoul(match[3].str());
    r.energy_in_mev = std::stod(match[4].str());

    logger::log(logger::LVL_INFO,
                "parsed: symbol:%s, A:%u, charge:%d, energy:%lf",
                atom->symbol().c_str(), iso->A, r.charge,
                r.energy_in_mev);

    return 0;
}

} // namespace isoparser
