#ifndef ISOTOPEPARSER_HH
#define ISOTOPEPARSER_HH

#include "Atom.hh"

#include <string>

namespace isoparser
{

struct Result
{
    qm::Atom::uint Z;
    qm::Atom::uint A;
    int charge;
    double energy_in_mev;
};

int parse(const std::string &str,
          const std::vector<qm::Atom> &atom_db, Result &r);

} // namespace isoparser

#endif // ISOTOPEPARSER_HH
