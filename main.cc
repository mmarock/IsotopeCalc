#include <algorithm>
#include <iomanip>
#include <iostream>
#include <memory>
#include <string>

#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstring>

#include <getopt.h>

#include "Atom.hh"
#include "AtomCalculator.hh"
#include "AtomImporter.hh"
#include "FileImport.hh"
#include "Logger.hh"

#include "isotope_parser.hh"

static void print_help(const char *name, const struct option *opts)
{
    std::cout << name << "\n";
    auto it = opts;
    while (it->name)
    {
        const char *arg = (it->has_arg) ? "arg required" : "";
        std::cout << "--" << it->name << "\t" << arg << "\n";
        ++it;
    }
}

static const char *get_symbol(const std::vector<qm::Atom> &atom_db,
                              qm::Atom::uint Z)
{

    auto it =
        std::find_if(atom_db.begin(), atom_db.end(),
                     [&](const qm::Atom &a) { return a.Z() == Z; });
    assert(it != atom_db.end() && "is end()");
    return it->symbol().c_str();
}

static void print_result_plot(
    const double &terminal_voltage, const double &magnetic_field,
    const double &injection, const double &magnet_radius,
    const qm::Atom &atom, isoparser::Result &parsed,
    const std::vector<qm::Calculator::Result> &result,
    const std::vector<qm::Atom> &atom_db, const bool &stable_only)
{
    std::cout << "Result:\n"
              << "=======\n"
              << std::left << std::setw(24) << "Terminal Voltage [MV]"
              << std::setw(3) << ":" << terminal_voltage << "\n"
              << std::setw(24) << "Magnetic Field [T]"
              << std::scientific << std::setw(3) << ":"
              << magnetic_field << std::defaultfloat << "\n"
              << std::setw(24) << "Magnet Radius [mm]" << std::setw(3)
              << ":" << magnet_radius << "\n"
              << std::setw(24) << "Injection [kV]" << std::setw(3)
              << ":" << injection << "\n"
              << std::setw(24) << "Reference Ion" << std::setw(3)
              << ":" << parsed.A << atom.symbol() << parsed.charge
              << "+(" << parsed.energy_in_mev << "MeV)\n"
              << std::endl;

    std::cout << '|' << std::setw(12) << "Ion" << '|' << std::setw(14)
              << "Energy [MeV]" << '|' << std::setw(12)
              << "Mass [amu]" << '|' << std::setw(18)
              << "Isotop Comp [%]" << '|' << std::setw(22)
              << "Mass Deviation [%]" << '|' << std::endl;
    std::size_t total(88);

    char line_buf[128];
    memset(line_buf, '-', sizeof(line_buf));
    line_buf[total] = '\0';
    std::cout << line_buf << std::endl;

    for (const auto &r : result)
    {
        if (r.isotope.isotopic_comp < 1E-2) {
            continue;
        }
        snprintf(line_buf, sizeof(line_buf), "%d%s%u+", r.isotope.A,
                 get_symbol(atom_db, r.Z), r.charge);
        std::cout << std::setprecision(3) << '|' << std::setw(12)
                  << line_buf << '|' << std::setw(14) << r.energy
                  << '|' << std::setw(12) << r.isotope.rel_atomic_mass
                  << '|' << std::setw(18)
                  << r.isotope.isotopic_comp * 1E2 << '|'
                  << std::setw(22) << std::scientific
                  << r.mass_deviation * 1E2 << std::defaultfloat
                  << '|' << std::endl;
    }
}

int main(int argc, char **argv)
{
    using namespace qm;
    using namespace logger;

    set_level(LVL_NOTICE);

    struct option opts[]{
        {"file", required_argument, NULL, 'f'},
        {"help", no_argument, NULL, 'h'},
        {"injection", required_argument, NULL, 'i'},
        {"isotope", required_argument, NULL, 'I'},
        {"magnet-radius", required_argument, NULL, 'r'},
        {"mass-error", required_argument, NULL, 'm'},
        {"stable-only", no_argument, NULL, 's'},
        {NULL, 0, NULL, 0}};

    std::string path_to_file("isotopes.txt");

    double magnet_radius(900);
    bool stable_only(false);
    double injection_voltage(80);
    double mass_error(0.1);

    std::string ion_str;

    int c(0);
    while ((c = getopt_long(argc, argv, "", opts, NULL)) > 0)
    {
        switch (c)
        {
        case 'f':
            path_to_file = optarg;
            break;
        case 'h':
            print_help(argv[0], opts);
            return 0;
        case 'i':
            injection_voltage = std::atof(optarg);
            break;
        case 'I':
            ion_str = optarg;
            break;
        case 'r':
            magnet_radius = std::atof(optarg);
            break;
        case 'm':
            mass_error = std::atof(optarg);
            break;
        case 's':
            stable_only = true;
            break;
        };
    }

    log(LVL_INFO,
        "use: magnet-radius:%lf, injection:%lf, mass-error:%lf, "
        "file:%s",
        magnet_radius, injection_voltage, mass_error,
        path_to_file.c_str());

    std::unique_ptr<AtomImporter> importer =
        std::make_unique<FileImport>("isotopes.txt");

    const auto atom_db(importer->read());

    isoparser::Result r;
    if (isoparser::parse(ion_str, atom_db, r))
    {
        return -1;
    }

    auto ion_it =
        std::find_if(atom_db.begin(), atom_db.end(),
                     [&](const Atom &a) { return a.Z() == r.Z; });
    if (ion_it == atom_db.end())
    {
        return -1;
    }

    const auto &isos = ion_it->isotopes();
    auto isotope_it =
        std::find_if(isos.begin(), isos.end(),
                     [&](const Isotope &i) { return i.A == r.A; });
    if (isotope_it == isos.end())
    {
        return -1;
    }
    log(LVL_INFO, "use: atom:%s, Z:%u, A:%u, mass:%.02lf",
        ion_it->symbol().c_str(), ion_it->Z(), isotope_it->A,
        isotope_it->rel_atomic_mass);

    auto ac = std::make_unique<AtomCalculator>(
        *ion_it, *isotope_it, r.charge, r.energy_in_mev,
        injection_voltage / 1000.0, magnet_radius / 1000.0,
        mass_error);

    std::vector<Calculator::Result> global_result;

    for (const auto &a : atom_db)
    {
        auto x = ac->query(a);
        global_result.insert(global_result.end(), x.begin(), x.end());
    }

    std::sort(global_result.begin(), global_result.end(),
              [](const Calculator::Result &lhs,
                 const Calculator::Result &rhs) {
                  return lhs.energy < rhs.energy;
              });

    for (const auto &y : global_result)
    {
        auto it =
            std::find_if(atom_db.begin(), atom_db.end(),
                         [&](const Atom &a) { return y.Z == a.Z(); });

        log(LVL_INFO,
            "%d%s %d+ Z:%u energy:%lf mass-dev:%lf%, "
            "isotopic-comp:%lf%",
            y.isotope.A, it->symbol().c_str(), y.charge, y.Z,
            y.energy, y.mass_deviation * 100.0,
            y.isotope.isotopic_comp * 100);
    }

    print_result_plot(ac->terminalVoltage(), ac->magneticField(),
                      injection_voltage, magnet_radius, *ion_it, r,
                      global_result, atom_db, stable_only);

    return 0;
}
