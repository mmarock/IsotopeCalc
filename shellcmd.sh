

#!/bin/sh

#./a.out --terminal-voltage 1800      \
#        --magnetic-field   0.449492 \
#        --magnet-radius    1000     \
#        --injection        78       \
#        --mass-err 1

#./a.out --terminal-voltage $1   \
#        --magnetic-field   $2   \
#        --magnet-radius    1000 \
#        --injection        $3   \
#        --mass-err 0.25

./isotope-calc --isotope $1         \
    --magnet-radius 1100            \
    --injection $2                  \
    --mass-err $3                   \
    --stable-only
